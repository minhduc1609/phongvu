﻿using ServiceProvider.Database;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace ServiceProvider.Controllers
{
    public class ProductRandomController : ApiController
    {
        // GET api/productrandom
        public dynamic Get()
        {
            return "12312312";
        }

        // GET api/productrandom/5
        public dynamic Get(int id)
        {
            DAOSanPham sp = new DAOSanPham();
            return sp.GetListSanPhamRandom(id);
        }

        // POST api/productrandom
        public void Post([FromBody]string value)
        {
        }

        // PUT api/productrandom/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/productrandom/5
        public void Delete(int id)
        {
        }
    }
}
