﻿using HtmlAgilityPack;
using ServiceProvider.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace ServiceProvider.Controllers
{
    public class RuttrichController : ApiController
    {
        // GET api/ruttrich
        public List<Deal> Get_tintuc()
        {
            try
            {

                HtmlWeb hw = new HtmlWeb();
                string url = @"http://phongvu.vn/";
                HtmlDocument doc = hw.Load(url);
                List<Deal> kq = new List<Deal>();
                var nodes = doc.DocumentNode.SelectNodes("//*[@class='content']//*[@class='leftCont']//*[@class='dongProducts']");
                if (nodes != null)
                {
                    for (int i = 0; i < nodes.Count; i++)
                    {

                        HtmlNodeCollection m = nodes[i].SelectNodes("//div//*[@class='boxsp']");
                        if (m != null)
                        {
                            for (int j = 0; j < 8; j++)
                            {
                                Deal de = new Deal();
                                string a = m[j].InnerHtml;
                                HtmlNode ab = HtmlNode.CreateNode(a);
                                de.title = ab.SelectSingleNode("//*[@class='picsp']//a").Attributes["title"].Value;
                                de.decription = url + ab.SelectSingleNode("//*[@class='picsp']//a").Attributes["href"].Value;
                                HtmlNode bg = ab.SelectSingleNode("//*[@class='picsp']//a").SelectSingleNode("img");
                                de.image = url + bg.Attributes["src"].Value;
                                de.money = ab.SelectSingleNode("//*[@class='giasp']").InnerText;
                                kq.Add(de);
                            }
                            return kq;
                        }

                    }
                }
                return null;
            }
            catch (Exception e)
            {
                return null;
            }

            
        }

        public List<Deal> Get_tintuc(string ten)
        {
            try
            {
                
                HtmlWeb hw = new HtmlWeb();
                string url = @"http://phongvu.vn/";
                HtmlDocument doc = hw.Load(url);
                List<Deal> kq = new List<Deal>();
                var nodes = doc.DocumentNode.SelectNodes("//*[@class='content']//*[@class='leftCont']//*[@class='dongProducts']");
                if (nodes != null)
                {
                    for (int i = 0; i < nodes.Count; i++)
                    {

                        HtmlNodeCollection m = nodes[i].SelectNodes("//div//*[@class='boxsp']");// nodes[i].ChildNodes.Elements("div");
                        if (m != null)
                        {
                            //foreach (HtmlNode n in m)
                            //{
                            for(int j=0;j<m.Count;j++)
                            {
                                
                                   //s= m[j];
                                //HtmlNodeCollection al1 = n.SelectNodes("//*[@class='picsp']//src");
                               // foreach (HtmlNode s in al)
                               // {

                                    Deal de = new Deal();
                                    string a = m[j].InnerHtml;
                                    HtmlNode ab = HtmlNode.CreateNode(a);
                                    de.title = ab.SelectSingleNode("//*[@class='picsp']//a").Attributes["title"].Value;
                                 de.decription =url+ ab.SelectSingleNode("//*[@class='picsp']//a").Attributes["href"].Value;
                                 HtmlNode bg = ab.SelectSingleNode("//*[@class='picsp']//a").SelectSingleNode("img");
                                 de.image = url + bg.Attributes["src"].Value;
                                 de.money = ab.SelectSingleNode("//*[@class='giasp']").InnerText;
                                   // de.money=tien.
                                    kq.Add(de);
                                //}
                                //return kq;
                                //de.title =  //item1.SelectSingleNode("a").Attributes["title"].Value;
                           }
                            return kq;
                        }
                        
                    }
                }
                //string html = de.GetHtmlWeb("http://phongvu.vn/");
                //HtmlAgilityPack.HtmlDocument document = new HtmlAgilityPack.HtmlDocument();
                //document.LoadHtml(html);
                //HtmlNode divNode;
                ////divNode = document.DocumentNode.SelectSingleNode("//*[@class='content']//*[@class='leftCont']");
                //List<Deal> kq = new List<Deal>();
                //var nodes=
                ////foreach (HtmlNode node in divNode.SelectNodes("//*[@class='dongProducts']"))
                ////{
                ////   //foreach( HtmlNode item in node.SelectNodes("//*[@class='boxsp']"))
                ////   // {
                ////        HtmlNode item = node.SelectSingleNode("//*[@class='boxsp']");
                ////        HtmlNode item1 = node.SelectSingleNode("//*[@class='picsp']");
                ////        de.title = item1.SelectSingleNode("a").Attributes["title"].Value;
                ////   // }
                ////}
                return null;
            }
            catch (Exception e)
            {
                return null;
            }
        }
    }
}
