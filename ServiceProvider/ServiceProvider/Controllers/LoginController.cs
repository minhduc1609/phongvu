﻿using Facebook;
using ServiceProvider.Database;
using ServiceProvider.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ServiceProvider.Controllers
{
    public class LoginController : Controller
    {
        //
        // GET: /Login/
        string host = "http://serviceprovider.apphb.com/Login";
        string appid = "565846006843207";

        public ActionResult Index()
        {
            if (Session["client_id"] == null && Session["redirect_uri"] == null && Session["scope"] == null)
            {
                Session["client_id"] = Request.QueryString["client_id"];
                Session["redirect_uri"] = Request.QueryString["redirect_uri"];
                Session["scope"] = Request.QueryString["scope"];
            }
            return View();
        }
        
        [HttpPost]
        public ActionResult LoginFacebook()
        {
            string stringfacebook = "https://www.facebook.com/dialog/oauth?client_id=" + appid
                + "&redirect_uri=" + host + "&scope=email,publish_stream, user_photos, friends_photos,read_stream";
            return Redirect(stringfacebook);
        }
        
        [HttpPost]
        public string ProcessFacebook(string accesstoken)
        {
            if (Session["info"] == null)
            {
                List<string> info = new List<string>();
                var fb = new FacebookClient(accesstoken);
                dynamic rs = fb.Get("/me?fields=name,id,email");
                info.Add(rs.id);
                info.Add(rs.name);
                info.Add(rs.email);
                Session["Accesstoken"] = accesstoken;
                Session["info"] = info;
                DAOUser us = new DAOUser();
                if (!us.CheckUser(rs.id))
                {
                    return "/Register/";
                }
                else
                {
                    return setAccessToken();
                }
            }
            return setAccessToken();
        }

        public string setAccessToken()
        {
            DAOUser us = new DAOUser();
            List<string> info = (List<string>)Session["info"];
            if (Session["client_id"] != null)
            {
                if (Session["client_id"].ToString() == InfomationApp.AppId)
                {
                    if (us.CheckAccesstoken(info[0])) //chưa nhận accesstoken từ app đó lần nào nên cần user cho phép
                    {
                        return "/Home/AcceptAccestoken/";
                    }
                    else
                    {
                        return Session["redirect_uri"].ToString()+"?code=" + us.GetAccesstoken(info[0]);
                    }
                }
                else
                {
                    return Session["redirect_uri"].ToString();
                }
            }
            return "1";
        }
        //
        // GET: /Login/Details/5

        public ActionResult Details(int id)
        {
            return View();
        }

        //
        // GET: /Login/Create

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /Login/Create

        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        //
        // GET: /Login/Edit/5

        public ActionResult Edit(int id)
        {
            return View();
        }

        //
        // POST: /Login/Edit/5

        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        //
        // GET: /Login/Delete/5

        public ActionResult Delete(int id)
        {
            return View();
        }

        //
        // POST: /Login/Delete/5

        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
