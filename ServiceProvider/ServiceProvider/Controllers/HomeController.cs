﻿using ServiceProvider.Database;
using ServiceProvider.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ServiceProvider.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index(FormCollection collection)
        {
            var x = Request.QueryString["Redirect_url"];
            var y = Request.QueryString["AppId"];
            return View();
        }

        public ActionResult Info(string id)
        {
            DAOUser us = new DAOUser();
            User user = us.Getuser(id);
            ViewBag.name = user.Username;
            ViewBag.email = user.Email; 
            return View();
        }

        public ActionResult ClientOAuth()
        {
            var x = Request.QueryString["Redirect_url"];
            var y = Request.QueryString["Scope"];
            if (x != null && y != null)
            {
                ViewBag.scope = y;
                ViewBag.Redirect_url = x;
            }
            return View();
        }

        public ActionResult AcceptAccestoken()
        {
            ViewBag.name = InfomationApp.name;
            ViewBag.scope = Session["scope"]; 
            return View();
        }

        [HttpPost]
        public ActionResult GetCode(FormCollection collection)
        {
            var x = collection["Redirect_url"];
            return View();
        }

        [HttpPost]
        public ActionResult SetCode(FormCollection collection)
        {
            if (collection["btn"] == "Yes")
            {
                List<string> info = (List<string>)Session["info"];
                Guid g = Guid.NewGuid();
                DateTime t = DateTime.Now;
                string scope = Session["scope"].ToString();
                DAOUser us = new DAOUser();
                if (us.Updateuser(t, g.ToString(), scope, info[0]))
                {
                    return Redirect(Session["redirect_uri"].ToString() + "?code=" + g);
                };
            }
            else
            {
                return Redirect(Session["redirect_uri"].ToString());
            }
            return View();
        }

        public ActionResult Logout()
        {
            
            string url_redis = Request.QueryString["redis"];
            if (url_redis == null)
            {
                url_redis = "http://serviceprovider.apphb.com/";
            }
            try
            {
                var oauth = new Facebook.FacebookClient(Session["Accesstoken"].ToString());

                var logoutParameters = new Dictionary<string, object>
                {
                    {"access_token", Session["Accesstoken"].ToString()},
                    { "next", url_redis }
                };
                Session.RemoveAll();
                var logoutUrl = oauth.GetLogoutUrl(logoutParameters);
                return Redirect(logoutUrl.AbsoluteUri);
            }
            catch {
                return Redirect(url_redis);
            }
        }

    }
}
