﻿using ServiceProvider.Database;
using ServiceProvider.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace ServiceProvider.Controllers
{
    public class ProductController : ApiController
    {
        // GET api/product
        public dynamic Get()
        {
            return "123123123";
        }

        // GET api/product/5
        public dynamic Get(int id)
        {
            DAOSanPham dbSP = new DAOSanPham();
            return dbSP.GetListSanPhamHot(id);
        }

        // POST api/product
        public void Post([FromBody]string value)
        {
        }

        // PUT api/product/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/product/5
        public void Delete(int id)
        {
        }
    }
}
