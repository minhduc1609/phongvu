﻿using ServiceProvider.Database;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace ServiceProvider.Controllers
{
    public class ProductSameController : ApiController
    {
        // GET api/productsame
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET api/productsame/5
        public dynamic Get(int id, string mansx)
        {
            DAOSanPham sp = new DAOSanPham();
            return sp.GetListSanPhamSame(id, mansx);
        }

        // POST api/productsame
        public void Post([FromBody]string value)
        {
        }

        // PUT api/productsame/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/productsame/5
        public void Delete(int id)
        {
        }
    }
}
