﻿using ServiceProvider.Database;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ServiceProvider.Controllers
{
    public class RegisterController : Controller
    {
        //
        // GET: /Register/

        public ActionResult Index()
        {
            List<string> info = (List<string>)Session["info"];
            ViewBag.email = info[2];
            return View(ViewBag);
        }

        //
        // GET: /Register/Details/5

        public ActionResult Reg(FormCollection collection)
        {
            var x = collection["btn"];
            if (x == "Yes")
            {
                if(Session["info"] != null)
                {
                    List<string> info = (List<string>)Session["info"];
                    DAOUser us = new DAOUser();
                    if (us.Insertuser(info))
                    {
                        return Redirect("/Home/Info/" + info[0]);
                    }
                }
            }
            return Redirect("/");
        }

        public ActionResult Details(int id)
        {
            return View();
        }

        //
        // GET: /Register/Create

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /Register/Create

        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        //
        // GET: /Register/Edit/5

        public ActionResult Edit(int id)
        {
            return View();
        }

        //
        // POST: /Register/Edit/5

        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        //
        // GET: /Register/Delete/5

        public ActionResult Delete(int id)
        {
            return View();
        }

        //
        // POST: /Register/Delete/5

        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
