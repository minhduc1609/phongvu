﻿using ServiceProvider.Database;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace ServiceProvider.Controllers
{
    public class InfoUserController : ApiController
    {
        // GET api/infouser
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET api/infouser/5
        public dynamic Get(string id)
        {
            DAOUser us = new DAOUser();
            if (us.CheckAccesstokenSame(id, "Info_user"))
            {
                return us.Getuseracc(id);
            }
            else
            {
                return "-1";
            }
        }

        // POST api/infouser
        public void Post([FromBody]string value)
        {
        }

        // PUT api/infouser/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/infouser/5
        public void Delete(int id)
        {
        }
    }
}
