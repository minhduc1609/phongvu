﻿using ServiceProvider.Database;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace ServiceProvider.Controllers
{
    public class DetailProductController : ApiController
    {
        // GET api/detailproduct
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET api/detailproduct/5
        public dynamic Get(int id)
        {
            DAOSanPham sp = new DAOSanPham();
            return sp.GetSanPham(id);
        }

        // POST api/detailproduct
        public void Post([FromBody]string value)
        {
        }

        // PUT api/detailproduct/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/detailproduct/5
        public void Delete(int id)
        {
        }
    }
}
