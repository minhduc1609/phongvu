﻿using ServiceProvider.Database;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace ServiceProvider.Controllers
{
    public class UserCheckController : ApiController
    {
        // GET api/usercheck
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET api/usercheck/5
        public dynamic Get(string id)
        {
            DAOUser us = new DAOUser();
            if (us.CheckUser(id))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        // POST api/usercheck
        public dynamic Post([FromBody]List<string> value)
        {
            DAOUser us = new DAOUser();
            if (us.Insertuser(value))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        // PUT api/usercheck/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/usercheck/5
        public void Delete(int id)
        {
        }
    }
}
