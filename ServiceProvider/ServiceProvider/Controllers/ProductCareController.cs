﻿using ServiceProvider.Database;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace ServiceProvider.Controllers
{
    public class ProductCareController : ApiController
    {
        // GET api/productcare
        public dynamic Get()
        {
            DAOSanPham dbSP = new DAOSanPham();
            return dbSP.GetListSanPhamQuanTam();
        }

        // GET api/productcare/5
        public string Get(int id)
        {
            return "value";
        }

        // POST api/productcare
        public void Post([FromBody]string value)
        {
        }

        // PUT api/productcare/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/productcare/5
        public void Delete(int id)
        {
        }
    }
}
