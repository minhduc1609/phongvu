﻿using ServiceProvider.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace ServiceProvider.Database
{
    public class DAOSanPham
    {
        SqlConnection conn = DataProvider.conn;
        SqlCommand command;
        public List<SanPham> GetListSanPhamHot(int sl)
        {
            List<SanPham> ListSP = new List<SanPham>();
            try
            {
                conn.Open();
                string sql = "Select Top " + sl + " * From SanPham sp, NhaSanXuat nsx Where sp.MaNSX = nsx.MaNSX order by sp.DiemBinhChon DESC";
                command = new SqlCommand(sql, conn);
                SqlDataReader reader = command.ExecuteReader();
                while(reader.Read())
                {
                    SanPham sp = new SanPham();
                    sp.MaSanPham = Int32.Parse(reader["MaSanPham"].ToString());
                    sp.TenSanPham = reader["TenSanPham"].ToString();
                    sp.MoTa = reader["MoTa"].ToString();
                    sp.DiemBinhChon = Int32.Parse(reader["DiemBinhChon"].ToString());
                    sp.NSXuat = new NhaSanXuat();
                    sp.NSXuat.MaNhaSanXuat = Int32.Parse(reader["MaNSX"].ToString());
                    sp.NSXuat.TenNhaSanXuat = reader["TenNSX"].ToString();
                    sp.Gia = Int32.Parse(reader["Gia"].ToString());
                    sp.SoLuong = Int32.Parse(reader["SoLuong"].ToString());
                    sp.AnhMinhHoa = reader["AnhMinhHoa"].ToString();
                    ListSP.Add(sp);
                }
                conn.Close();
                return ListSP;
            }
            catch
            {
 
            }
       
            return ListSP;
        }

        public List<SanPham> GetListSanPhamQuanTam()
        {
            List<SanPham> ListSP = new List<SanPham>();
            try
            {
                conn.Open();
                string sql = "Select Top 8 * From SanPham sp, NhaSanXuat nsx Where sp.MaNSX = nsx.MaNSX order by sp.Gia";
                command = new SqlCommand(sql, conn);
                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    SanPham sp = new SanPham();
                    sp.MaSanPham = Int32.Parse(reader["MaSanPham"].ToString());
                    sp.TenSanPham = reader["TenSanPham"].ToString();
                    sp.MoTa = reader["MoTa"].ToString();
                    sp.DiemBinhChon = Int32.Parse(reader["DiemBinhChon"].ToString());
                    sp.NSXuat = new NhaSanXuat();
                    sp.NSXuat.MaNhaSanXuat = Int32.Parse(reader["MaNSX"].ToString());
                    sp.NSXuat.TenNhaSanXuat = reader["TenNSX"].ToString();
                    sp.Gia = Int32.Parse(reader["Gia"].ToString());
                    sp.SoLuong = Int32.Parse(reader["SoLuong"].ToString());
                    sp.AnhMinhHoa = reader["AnhMinhHoa"].ToString();
                    ListSP.Add(sp);
                }
                conn.Close();
                return ListSP;
            }
            catch
            {

            }

            return ListSP;
        }

        public SanPham GetSanPham(int id)
        {
            SanPham sp = new SanPham();
            try
            {
                conn.Open();
                string sql = "Select * From SanPham sp, NhaSanXuat nsx Where sp.MaNSX = nsx.MaNSX and sp.MaSanPham = " + id;
                command = new SqlCommand(sql, conn);
                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    sp.MaSanPham = Int32.Parse(reader["MaSanPham"].ToString());
                    sp.TenSanPham = reader["TenSanPham"].ToString();
                    sp.MoTa = reader["MoTa"].ToString();
                    sp.DiemBinhChon = Int32.Parse(reader["DiemBinhChon"].ToString());
                    sp.NSXuat = new NhaSanXuat();
                    sp.NSXuat.MaNhaSanXuat = Int32.Parse(reader["MaNSX"].ToString());
                    sp.NSXuat.TenNhaSanXuat = reader["TenNSX"].ToString();
                    sp.Gia = Int32.Parse(reader["Gia"].ToString());
                    sp.SoLuong = Int32.Parse(reader["SoLuong"].ToString());
                    sp.AnhMinhHoa = reader["AnhMinhHoa"].ToString();
                }
                conn.Close();
                return sp;
            }
            catch
            {

            }

            return sp;
        }

        public List<SanPham> GetListSanPhamRandom(int sl)
        {
            List<SanPham> ListSP = new List<SanPham>();
            try
            {
                conn.Open();
                string sql = "SELECT TOP "+sl+" * FROM SanPham sp, NhaSanXuat nsx WHERE sp.MaNSX = nsx.MaNSX ORDER BY NEWID()";
                command = new SqlCommand(sql, conn);
                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    SanPham sp = new SanPham();
                    sp.MaSanPham = Int32.Parse(reader["MaSanPham"].ToString());
                    sp.TenSanPham = reader["TenSanPham"].ToString();
                    sp.MoTa = reader["MoTa"].ToString();
                    sp.DiemBinhChon = Int32.Parse(reader["DiemBinhChon"].ToString());
                    sp.NSXuat = new NhaSanXuat();
                    sp.NSXuat.MaNhaSanXuat = Int32.Parse(reader["MaNSX"].ToString());
                    sp.NSXuat.TenNhaSanXuat = reader["TenNSX"].ToString();
                    sp.Gia = Int32.Parse(reader["Gia"].ToString());
                    sp.SoLuong = Int32.Parse(reader["SoLuong"].ToString());
                    sp.AnhMinhHoa = reader["AnhMinhHoa"].ToString();
                    ListSP.Add(sp);
                }
                conn.Close();
                return ListSP;
            }
            catch
            {

            }

            return ListSP;
        }

        public List<SanPham> GetListSanPhamSame(int sl, string MaNhaSanXuat)
        {
            List<SanPham> ListSP = new List<SanPham>();
            try
            {
                string sql;
                conn.Open();
                if (sl == 0)
                {
                    sql = "SELECT * FROM SanPham sp, NhaSanXuat nsx WHERE sp.MaNSX = nsx.MaNSX and nsx.MaNSX =" + MaNhaSanXuat;
                }
                else
                {
                    sql = "SELECT TOP " + sl + " * FROM SanPham sp, NhaSanXuat nsx WHERE sp.MaNSX = nsx.MaNSX and nsx.MaNSX =" + MaNhaSanXuat;
                }
                command = new SqlCommand(sql, conn);
                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    SanPham sp = new SanPham();
                    sp.MaSanPham = Int32.Parse(reader["MaSanPham"].ToString());
                    sp.TenSanPham = reader["TenSanPham"].ToString();
                    sp.MoTa = reader["MoTa"].ToString();
                    sp.DiemBinhChon = Int32.Parse(reader["DiemBinhChon"].ToString());
                    sp.NSXuat = new NhaSanXuat();
                    sp.NSXuat.MaNhaSanXuat = Int32.Parse(reader["MaNSX"].ToString());
                    sp.NSXuat.TenNhaSanXuat = reader["TenNSX"].ToString();
                    sp.Gia = Int32.Parse(reader["Gia"].ToString());
                    sp.SoLuong = Int32.Parse(reader["SoLuong"].ToString());
                    sp.AnhMinhHoa = reader["AnhMinhHoa"].ToString();
                    ListSP.Add(sp);
                }
                conn.Close();
                return ListSP;
            }
            catch
            {

            }

            return ListSP;
        }
    }
}

