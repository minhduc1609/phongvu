﻿using ServiceProvider.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace ServiceProvider.Database
{
    public class DAOUser
    {
        SqlConnection conn = DataProvider.conn;
        SqlCommand command;
        public bool CheckUser(string id)
        {
            try
            {
                conn.Open();
                string sql = "Select * From TaiKhoan Where ID = "+id;
                command = new SqlCommand(sql, conn);
                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    conn.Close();
                    return true;
                }
            }
            catch
            {

            }
            conn.Close();
            return false;
        }

        public bool Insertuser(List<string> id)
        {
            try
            {
                conn.Open();
                string sql = "INSERT INTO TaiKhoan([ID],[Username],[Email]) VALUES('" + id[0] + "','" + id[1] + "','" + id[2] + "')";
                command = new SqlCommand(sql, conn);
                command.ExecuteNonQuery();
                conn.Close();
                return true;
            }
            catch(Exception e)
            {

            }
            conn.Close();
            return false;
        }

        public User Getuser(string id)
        {
            User us = new User();
            try
            {
                conn.Open();
                string sql = "Select * From TaiKhoan Where ID = " + id;
                command = new SqlCommand(sql, conn);
                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    us.ID = reader["ID"].ToString();
                    us.Username = reader["Username"].ToString();
                    us.Email = reader["Email"].ToString();
                    conn.Close();
                    return us;
                }
            }
            catch
            {

            }
            conn.Close();
            return us;
        }

        public User Getuseracc(string acc)
        {
            User us = new User();
            try
            {
                conn.Open();
                string sql = "Select * From TaiKhoan Where CodeAccesstoken LIKE '" + acc + "'";
                command = new SqlCommand(sql, conn);
                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    us.ID = reader["ID"].ToString();
                    us.Username = reader["Username"].ToString();
                    us.Email = reader["Email"].ToString();
                    conn.Close();
                    return us;
                }
            }
            catch
            {

            }
            conn.Close();
            return us;
        }

        public bool CheckAccesstoken(string id)
        {
            User us = new User();
            try
            {
                conn.Open();
                string sql = "Select CodeAccesstoken From TaiKhoan Where ID = " + id;
                command = new SqlCommand(sql, conn);
                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    try
                    {
                        string s = reader["CodeAccesstoken"].ToString();
                        if (s != "")
                        {
                            conn.Close();
                            return false;
                        }
                    }
                    catch
                    {

                    }
                }
            }
            catch
            {

            }
            conn.Close();
            return true;
        }

        public string GetAccesstoken(string id)
        {
            string acc = null;
            try
            {
                conn.Open();
                string sql = "Select CodeAccesstoken From TaiKhoan Where ID = " + id;
                command = new SqlCommand(sql, conn);
                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    acc = reader["CodeAccesstoken"].ToString();
                    conn.Close();
                    return acc;
                }
            }
            catch
            {

            }
            conn.Close();
            return acc;
        }

        public bool CheckAccesstokenSame(string accesstoken, string scope)
        {
            User us = new User();
            try
            {
                conn.Open();
                string sql = "Select CodeAccesstoken From TaiKhoan Where CodeAccesstoken LIKE '"+accesstoken+"' And LinkApi LIKE '"+scope+"'";
                command = new SqlCommand(sql, conn);
                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    conn.Close();
                    return true;
                }
            }
            catch (Exception e)
            {

            }
            conn.Close();
            return false;
        }

        public bool Updateuser(DateTime t, string g, string s, string id)
        {
            try
            {
                conn.Open();
                string sql = "UPDATE TaiKhoan SET [NgayGio] = '" + t.ToString() + "',[LinkApi] = '" + s + "',[CodeAccesstoken] = '" + g + "' WHERE [ID] = " + id;
                command = new SqlCommand(sql, conn);
                command.ExecuteNonQuery();
                conn.Close();
                return true;
            }
            catch
            {

            }
            conn.Close();
            return false;
        }
    }
}