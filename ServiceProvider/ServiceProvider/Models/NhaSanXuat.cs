﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ServiceProvider.Models
{
    public class NhaSanXuat
    {
        public int MaNhaSanXuat { get; set; }
        public string TenNhaSanXuat { get; set; }
    }
}