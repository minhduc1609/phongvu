﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;

namespace ServiceProvider.Models
{
    public class Deal
    {
        public string title { get; set; }
        public string decription { get; set; }
        public string image { get; set; }
        public string money { get; set; }
       
        
        
        public string link { get; set; }
        



        public string GetHtmlWeb(string web)
        {
            try
            {
                WebRequest wr = WebRequest.Create(web);
                WebResponse wp = wr.GetResponse();
                Stream ReceiveStream = wp.GetResponseStream();

                Encoding encode = System.Text.Encoding.GetEncoding("utf-8");
                StreamReader readStream = new StreamReader(ReceiveStream, encode);
                string html = readStream.ReadToEnd();
                return html;
            }
            catch { return ""; }
        }
    }
  
}