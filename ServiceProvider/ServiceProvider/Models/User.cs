﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ServiceProvider.Models
{
    public class User
    {
        public string ID { get; set; }
        public string Username { get; set; }
        public DateTime NgayGio { get; set; }
        public string LinkApi { get; set; }
        public string CodeAccessToken { get; set; }
        public string Email { get; set; }
    }
}