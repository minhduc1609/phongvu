﻿var access_Token;
(function (d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s); js.id = id;
    js.src = "//connect.facebook.net/en_US/all.js#xfbml=1&appId=565846006843207";
    fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));
window.fbAsyncInit = function () {
    FB.init({
        appId: '565846006843207', // App ID
        channelUrl: '//' + window.location.hostname + '/channel', // Path to your Channel File
        status: true, // check login status
        cookie: true, // enable cookies to allow the server to access the session
        xfbml: true  // parse XFBML
    });
    // listen for and handle auth.statusChange events
    FB.Event.subscribe('auth.statusChange', function (response) {
        if (response.authResponse) {
            // user has auth'd your app and is logged into Facebook
            var uid = response.authResponse.userID;
            access_Token = response.authResponse.accessToken;
            $.post('/Login/ProcessFacebook/', { accesstoken: access_Token }, function (result) {
                if (result != "1") {
                    window.location = result;
                }
            });
            //FB.api('/me', function (me) {
            //    if (me.name) {
            //        document.getElementById('auth-displayname').innerHTML = me.name;
            //        document.getElementById('auth-displaypicture').innerHTML = "<img src='http://graph.facebook.com/" + me.id + "/picture' width = 40 height = 40></img>";
            //    }
            //    //ko.applyBindings(new viewmodel());
            //});
            //$('#auth-loggedout').css('display', 'none');
            //$('#auth-loggedin').css('display', 'block');
            $('#login').css('display', 'none');
            $('#logout').css('display', 'block');
        } else {
            // user has not auth'd your app, or is not logged into Facebook
        }
    });
    $("#logout").click(function () { DangXuat(); FB.logout(function () { window.location = "/"; }); });
}

function DangXuat() {
    //$('#auth-loggedout').css('display', 'block');
    //$('#auth-loggedin').css('display', 'none');
    $('#login').css('display', 'block');
    $('#logout').css('display', 'none');
    //$.get('/Home/ProcessLogout/', function (result) { });
}

function StartLoad() {
    if (access_Token == null) {
        //$('#auth-loggedout').css('display', 'block');
        //$('#auth-loggedin').css('display', 'none');
        $('#login').css('display', 'block');
        $('#logout').css('display', 'none');
    }
}

