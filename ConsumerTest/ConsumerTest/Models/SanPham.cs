﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ServiceProvider.Models
{
    public class SanPham
    {
        public int MaSanPham{ get; set;}
        public string TenSanPham { get; set; }
        public string MoTa { get; set; }
        public int DiemBinhChon { get; set; }
        public NhaSanXuat NSXuat { get; set; }
        public int Gia { get; set; }
        public int SoLuong { get; set; }
        public string AnhMinhHoa { get; set; }
    }
}