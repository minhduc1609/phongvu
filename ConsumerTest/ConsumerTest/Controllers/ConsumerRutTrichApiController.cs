﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web.Http;

namespace ConsumerTest.Controllers
{
    public class ConsumerRutTrichApiController : ApiController
    {
        // GET api/consumerruttrichapi
        public async Task<dynamic> Get()
        {
            dynamic product = null;
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("http://serviceprovider.apphb.com/");
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                // New code:
                HttpResponseMessage response = await client.GetAsync("/api/ruttrich/");
                if (response.IsSuccessStatusCode)
                {
                    product = await response.Content.ReadAsAsync<dynamic>();
                }
            }
            return product;
        }

        // GET api/consumerruttrichapi/5
        public string Get(int id)
        {
            return "value";
        }

        // POST api/consumerruttrichapi
        public void Post([FromBody]string value)
        {
        }

        // PUT api/consumerruttrichapi/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/consumerruttrichapi/5
        public void Delete(int id)
        {
        }
    }
}
