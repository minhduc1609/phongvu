﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web.Http;

namespace ConsumerTest.Controllers
{
    public class ConsumerProductRandomAPIController : ApiController
    {
        // GET api/consumerproductrandomapi
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET api/consumerproductrandomapi/5
        public async Task<dynamic> Get(int id)
        {
            dynamic product = null;
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("http://serviceprovider.apphb.com/");
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                // New code:
                HttpResponseMessage response = await client.GetAsync("/api/productrandom/"+id);
                if (response.IsSuccessStatusCode)
                {
                    product = await response.Content.ReadAsAsync<dynamic>();
                }
            }
            return product;
        }

        // POST api/consumerproductrandomapi
        public void Post([FromBody]string value)
        {
        }

        // PUT api/consumerproductrandomapi/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/consumerproductrandomapi/5
        public void Delete(int id)
        {
        }
    }
}
