﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web.Http;

namespace ConsumerTest.Controllers
{
    public class ConsumerUserAPIController : ApiController
    {
        // GET api/consumeruserapi
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET api/consumeruserapi/5
        public async Task<dynamic> Get(string id)
        {
            dynamic check = null;
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("http://serviceprovider.apphb.com/");
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                // New code:
                HttpResponseMessage response = await client.GetAsync("api/infouser/" + id);
                if (response.IsSuccessStatusCode)
                {
                    check = await response.Content.ReadAsAsync<dynamic>();
                }
            }
            return check;
        }

        // POST api/consumeruserapi
        public async Task<dynamic> Post([FromBody]List<string> value)
        {
            dynamic resp = null;
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("http://serviceprovider.apphb.com/");
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                var vl = new List<string>();
                vl.Add(value[0]);
                vl.Add(value[1]);
                vl.Add(value[2]);
                HttpResponseMessage response = await client.PostAsJsonAsync<dynamic>("api/usercheck", vl);
                if (response.IsSuccessStatusCode)
                {
                   resp  = await response.Content.ReadAsAsync<dynamic>();
                }
            }
            return resp;
        }

        // PUT api/consumeruserapi/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/consumeruserapi/5
        public void Delete(int id)
        {
        }
    }
}
