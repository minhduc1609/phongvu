﻿using Facebook;
using Microsoft.Web.WebPages.OAuth;
using ServiceProvider.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace ConsumerTest.Controllershttp
{
    public class HomeController : Controller
    {
        string appid = "263955560426222";
        string appsecret = "5dd0a6e1237e18662008c428c3f29fde";
        string host = "http://www.haivl.com/";
        string code;
        public ActionResult Index()
        {
            var x = Request.QueryString["code"];
            if (x != null)
            {
                Session["accesstoken"] = x;
            }
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your app description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        [HttpGet]
        public void ProcessLogout()
        {
            Session.RemoveAll();
        }
    }
}
