﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace ConsumerTest.Controllers
{
    public class GetAccessTokenController : Controller
    {
        //
        // GET: /GetAccessToken/

        public ActionResult Index()
        {
            return View();
        }

        //
        // GET: /GetAccessToken/Details/5

        public ActionResult Details(int id)
        {
            return View();
        }

        //
        // GET: /GetAccessToken/Create

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /GetAccessToken/Create

        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        //
        // GET: /GetAccessToken/Edit/5

        public ActionResult Edit(int id)
        {
            return View();
        }

        //
        // POST: /GetAccessToken/Edit/5

        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        //
        // GET: /GetAccessToken/Delete/5

        public ActionResult Delete(int id)
        {
            return View();
        }

        //
        // POST: /GetAccessToken/Delete/5

        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        [HttpPost]
        public ActionResult GetAccess(FormCollection collection)
        {
            string url = "";
            if (!string.IsNullOrEmpty(collection["SelectedGender"]))
            {
                string checkResp = collection["SelectedGender"];
                url = "http://localhost:42134/Home/ClientOAuth?Scope=" + checkResp + "&Redirect_url=http://localhost:42134/";
                if (Session["IdUser"] == null)
                {
                    url = "/";
                }
                else
                {
                    
                }
            }
            return Redirect(url);
        }
    }
}
