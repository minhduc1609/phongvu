﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace ConsumerTest.Controllers
{
    public class ConsumerAccessTokenApiController : ApiController
    {
        // GET api/consumeraccesstokenapi
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET api/consumeraccesstokenapi/5
        public string Get(int id)
        {
            return "value";
        }

        // POST api/consumeraccesstokenapi
        public void Post([FromBody]string value)
        {
        }

        // PUT api/consumeraccesstokenapi/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/consumeraccesstokenapi/5
        public void Delete(int id)
        {
        }
    }
}
