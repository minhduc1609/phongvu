﻿function SetProduct(data) {
    var self = this;
    self.MaSanPham = data.MaSanPham;
    self.Url = "/DetailProduct/Details/"+data.MaSanPham;
    self.TenSanPham = data.TenSanPham || "";
    self.MoTa = data.MoTa || "";
    self.Gia = data.Gia + " VNĐ" || "";
    self.AnhMinhHoa = "http://serviceprovider.apphb.com/" + data.AnhMinhHoa || "";
}

function SetProductRuttrich(data) {
    var self = this;
    self.title = data.title;
    self.Url = data.decription;
    self.AnhMinhHoa = data.image;
    self.Gia = data.money;
}

function viewmodel()
{
    initfb();
    var self = this;
    self.product = ko.observableArray();
    self.product2 = ko.observableArray();
    self.product3 = ko.observableArray();
    self.product4 = ko.observableArray();
    self.productRuttrich1 = ko.observableArray();
    self.productRuttrich2 = ko.observableArray();
    self.newMessage = ko.observable();
    self.error = ko.observable();

    self.loadProductHot = function () {
        $.ajax({
            url: 'http://consumertest.apphb.com/api/consumerproductapi',
            dataType: "json",
            type: 'GET',
        })
        .done(function (data) {
            var mappedProduct = $.map(data, function (item) { return new SetProduct(item); });
            $.each(mappedProduct, function (index, item) {
                if (index > 3) {
                    self.product2.push(item);
                }
                else {
                    self.product.push(item);
                }
            })
            self.loadProductCare();
        })
        .fail(function () {
            error('unable to load posts');
        });
    };

    self.loadProductCare = function () {
        $.ajax({
            url: 'http://consumertest.apphb.com/api/consumerproductcareapi',
            dataType: "json",
            type: 'GET',
        })
        .done(function (data) {
            var mappedProduct = $.map(data, function (item) { return new SetProduct(item); });
            $.each(mappedProduct, function (index, item) {
                if (index > 3) {
                    self.product4.push(item);
                }
                else {
                    self.product3.push(item);
                }
            })
            self.loadRuttrich();

        })
        .fail(function () {
            error('unable to load posts');
        });
    };

    self.loadRuttrich = function () {
        $.ajax({
            url: 'http://consumertest.apphb.com/api/consumerruttrichapi',
            dataType: "json",
            type: 'GET',
        })
        .done(function (data) {
            var mappedProduct = $.map(data, function (item) { return new SetProductRuttrich(item); });
            $.each(mappedProduct, function (index, item) {
                if (index <= 3) {
                    self.productRuttrich1.push(item);
                }
                else {
                    self.productRuttrich2.push(item);
                }
                
            })
        })
        .fail(function () {
            error('unable to load posts');
        });
    };

    self.loadProductHot();
    return self;
}

ko.applyBindings(new viewmodel());