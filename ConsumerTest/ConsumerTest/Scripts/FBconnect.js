﻿var data_Response;
var linkPostNext = "";
var flagMorePost = 1;
var access_Token = sessionStorage.Accesstoken;
var flagLoadSortLike = 0;
var pageLoadSortLike = 0;
var pageLoadSortComment = 0;
var flagLoadSortComment = 0;
var pageLoadHashTag = 0;
var flagLoadHashTag = 0;
var nextPageLoadSort = null;
var flagCommentorLike = null;
var flagLoadHashTagorPost = 0;
var txtHashTag = null;


//Model các thông tin của một Post
function Post(data) {
    var self = this;
    var numberComment = 0;
    var numberCommentIsShow = 0;
    var idnextcomment = data.id;
    var flag = 0;
    data = data || {};
    self.idDivComment = data.id;
    self.PostId = data.from.id || "";
    self.Message = data.message || "";
    self.PostedBy = data.PostedBy  || "";
    self.Url = data.from.Link || "#";
    self.PostedByName = data.from.name || "";
    self.PostedByAvatar = data.from.ImageOfPost || "";
    self.PostPicture = data.picture || "";
    self.txtUrl = "";
    self.idNumberofCommnent = 'idDiv' + data.id;
    self.idDivViewComment = 'idDivViewComment' + data.id;
    self.UrlWhoLike = "#";
    if (data.likes) {
        try
        {
            self.txtNumberLike = data.likes.data.length + " people";
        }
        catch (err)
        {
            self.txtNumberLike = data.likecount + " people";
        }
    }
    else {
        self.txtNumberLike = data.likecount || 0 + " people";
    }
    //self.error = ko.observable();
    self.PostedDate = getTimeAgo(data.created_time);
    self.PostComments = ko.observableArray();
    self.idComment = data.id;
    self.newCommentMessage = ko.observable();
    self.addComment = function () {
        var comment = new Comment();
        comment.PostId = self.PostId;
        comment.Message(self.newCommentMessage());
        return $.ajax({
            url: commentApiUrl,
            dataType: "json",
            contentType: "application/json",
            cache: false,
            type: 'POST',
            data: ko.toJSON(comment)
        })
        .done(function (result) {
            self.PostComments.push(new Comment(result));
            self.newCommentMessage('');
        })
        .fail(function () {
            error('unable to add post');
        });
    }

    if (data.comments) {
        try
        {
            numberComment = data.comments.summary.total_count;
        }
        catch (ex)
        {
            numberComment = data.commentcount;
        }
        numberCommentIsShow = data.comments.data.length;
        if (numberComment !== numberCommentIsShow)
        {
            self.txtUrl = "View more comment";
        }
        var datcmt = data.comments.data;
        var mappedPosts = $.map(datcmt, function (item) { return new Comment(item); });
        self.PostComments(mappedPosts);
    }
    self.txtNumberofCommnent  = numberCommentIsShow + " of " + numberComment;

    self.toggleComment = function (item, event) {
        $(event.target).next().find('.publishComment').toggle();
    }

    /*---------------------------------------------------------*/
    self.loadMoreComment = function () {
        var iddivcomment = data.id;
        $('#' + iddivcomment + '').css('display', 'block');
        var vl = {
            'id': idnextcomment,
            'numbercomment' : numberComment,
            'AccessToken': access_Token
        }
        $.ajax({
            url: '../api/loadmorecomment/',
            dataType: "json",
            type: 'GET',
            data: vl
        })
        .done(function (data) {
            var i = 0;
            var mappedComment = $.map(data.data, function (item) { return new Comment(item); });
            $.each(mappedComment, function (index, item) {
                if (i < 3) {
                    i++;
                }
                else {
                    self.PostComments.push(item);
                }
            })
            $('#' + iddivcomment + '').css('display', 'none');
            numberCommentIsShow = data.data.length;
            $('#' + self.idDivViewComment).text('');
            $('#' + self.idNumberofCommnent).text(numberCommentIsShow + " of " + numberComment);
        })
        .fail(function () {
            error('unable to load posts');
        });
    }

    self.clickHashTag = function () {
        alert(' 123 ');
    }
}



function Comment(data) {
    var self = this;
    data = data || {};

    /// Persisted properties
    self.CommentId = data.CommentId;
    self.PostId = data.PostId;
    self.Message = ko.observable(data.message || "");
    self.CommentedBy = data.CommentedBy || "";
    self.CommentedByAvatar = data.from.ImageOfComment || "";
    self.CommentedByName = data.from.name || "";
    self.CmtUrl = data.from.Link || "#";
    self.CommentedDate = getTimeAgo(data.created_time);
    self.error = ko.observable();
    self.likecountcomment = data.like_count || 0;
    try
    {
        self.Imagecommnet = data.attachment.media.image.src; 
    }
    catch(ex)
    {
        self.Imagecommnet = "";
    }
    // persist edits to real values on accept
    self.deleteComment = function () {

    }

}

function getTimeAgo(varDate) {
    if (varDate) {
        return $.timeago(varDate.toString().slice(-1) == 'Z' ? varDate : varDate + 'Z');
    }
    else {
        return '';
    }
}
function InforME(data)
{
    var self = this;
    self.profile = data.profile;
    self.nameme = data.name;
    self.urlme = data.link;
}
function viewModel() {
    var self = this;
    self.posts = ko.observableArray();
    self.newMessage = ko.observable();
    self.error = ko.observable();
    self.Informe = ko.observable();
    self.getMe = function () {
        var vl = {
            'AccessToken': access_Token
        };
        $.ajax({
            url: '../api/me/',
            dataType: "json",
            type: 'GET',
            data: vl,
        })
        .done(function (data) {
            self.Informe(new InforME(data));
        })
        .fail(function () {
            error('unable to load posts');
        });
    };

    self.loadPosts = function () {
        allpost();
        var vl = {
            'value': linkPostNext,
            'AccessToken': access_Token
        }
        $.ajax({
            url: '../api/loadpostvscomment/',
            dataType: "json",
            type: 'GET',
            data: vl,

        })
        .done(function (data) {
            var mappedPosts = $.map(data, function (item) { return new Post(item); });
            self.posts(mappedPosts);
            linkPostNext = data[0].paging.next;
            $("#loading").css("display", "none");
        })
        .fail(function () {
            error('unable to load posts');
        });
    };

    self.LoadMorePost = function () {
        var data_Resp;
        var vl = {
            'value': linkPostNext,
            'AccessToken': access_Token
        };
        //To load existing posts
        $.ajax({
            url: '../api/loadpostvscomment/',
            dataType: "json",
            type: 'GET',
            data: vl
        })
        .done(function (data) {
            var mappedPosts = $.map(data, function (item) { return new Post(item); });
            $.each(mappedPosts, function (index, item) {
                self.posts.push(item);
            })
            flagMorePost = 1;
            linkPostNext = data[0].paging.next;
            $('#loadingMorePost').css('display', 'none');
        })
        .fail(function () {
            error('unable to load posts');
        });
    };

    self.LoadSortLike = function () {
        sortlike();
        flagMorePost = 0;
        flagCommentorLike = 0;
        pageLoadSortLike = 0;
        flagLoadSortLike = 1;
        self.posts.removeAll();
        $('#msgHolder').css("display", "none");
        $("#loading").css("display", "block");
        var vl = {
            'page': pageLoadSortLike,
            'AccessToken': access_Token,
            'flagCommentorLike': flagCommentorLike
        };
        $.ajax({
            url: '../api/loadpostusingsort/',
            dataType: "json",
            type: 'GET',
            data: vl
        })
        .done(function (data) {
            var mappedPosts = $.map(data, function (item) { return new Post(item); });
            self.posts(mappedPosts);
            //linkPostNext = data[0].paging.next;
            flagLoadSortLike = 1;
            $('#loading').css('display', 'none');
            $('#msgHolder').css("display", "block");
            pageLoadSortLike++;
            nextPageLoadSort = data[data.length - 1].nextpage;
        })
        .fail(function () {
            error('unable to load posts');
        });
    };


    self.LoadSortComment = function () {
        sortcomment();
        flagMorePost = 0;
        flagCommentorLike = 1;
        pageLoadSortComment = 0;
        flagLoadSortComment = 1;
        self.posts.removeAll();
        $('#msgHolder').css("display", "none");
        $("#loading").css("display", "block");
        var vl = {
            'page': pageLoadSortLike,
            'AccessToken': access_Token,
            'flagCommentorLike': flagCommentorLike
        };
        $.ajax({
            url: '../api/loadpostusingsort/',
            dataType: "json",
            type: 'GET',
            data: vl
        })
        .done(function (data) {
            var mappedPosts = $.map(data, function (item) { return new Post(item); });
            self.posts(mappedPosts);
            //linkPostNext = data[0].paging.next;
            flagLoadSortComment = 1;
            $('#loading').css('display', 'none');
            $('#msgHolder').css("display", "block");
            pageLoadSortComment++;
            nextPageLoadSort = data[data.length - 1].nextpage;
        })
        .fail(function () {
            error('unable to load posts');
        });
    };


    self.LoadMorePostSortLike = function () {
        var count = pageLoadSortLike * 10;
        var postid = new Array();
        var likecount = new Array();
        var commentcount = new Array();
        for (var i = 0; i < 10; i++) {
            try {
                postid[i] = nextPageLoadSort.data[0].fql_result_set[i + count].post_id;
                likecount[i] = nextPageLoadSort.data[0].fql_result_set[i + count].likes.count;
                commentcount[i] = nextPageLoadSort.data[0].fql_result_set[i + count].comment_info.comment_count;
            }
            catch (ex) {

            }
        }
        if (postid.length != 0) {
            var vl = {
                'page': pageLoadSortLike,
                'postid': postid,
                'likecount': likecount,
                'commentcount': commentcount,
                'AccessToken': access_Token,
                'flagCommentorLike': flagCommentorLike
            };
            //To load existing posts
            $.ajax({
                url: '../api/loadpostusingsort/',
                dataType: "json",
                type: 'GET',
                data: vl
            })
            .done(function (data) {
                var mappedPosts = $.map(data, function (item) { return new Post(item); });
                $.each(mappedPosts, function (index, item) {
                    self.posts.push(item);
                })
                flagLoadSortLike = 1;
                $('#loadingMorePost').css('display', 'none');
                pageLoadSortLike++;
            })
            .fail(function () {
                error('unable to load posts');
            });
        }
        else {
            $('#loadingMorePost').css('display', 'none');
        }
    };

    self.LoadMorePostSortComment = function () {
        var count = pageLoadSortComment * 10;
        var postid = new Array();
        var likecount = new Array();
        var commentcount = new Array();
        for (var i = 0; i < 10; i++) {
            try {
                postid[i] = nextPageLoadSort.data[0].fql_result_set[i + count].post_id;
                likecount[i] = nextPageLoadSort.data[0].fql_result_set[i + count].likes.count;
                commentcount[i] = nextPageLoadSort.data[0].fql_result_set[i + count].comment_info.comment_count;
            }
            catch (ex) {

            }
        }
        if (postid.length != 0) {
            var vl = {
                'page': pageLoadSortComment,
                'postid': postid,
                'likecount': likecount,
                'commentcount': commentcount,
                'AccessToken': access_Token,
                'flagCommentorLike': flagCommentorLike
            };
            //To load existing posts
            $.ajax({
                url: '../api/loadpostusingsort/',
                dataType: "json",
                type: 'GET',
                data: vl
            })
            .done(function (data) {
                var mappedPosts = $.map(data, function (item) { return new Post(item); });
                $.each(mappedPosts, function (index, item) {
                    self.posts.push(item);
                })
                flagLoadSortComment = 1;
                $('#loadingMorePost').css('display', 'none');
                pageLoadSortComment++;
            })
            .fail(function () {
                error('unable to load posts');
            });
        }
        else {
            $('#loadingMorePost').css('display', 'none');
        }
    };

    self.LoadHashTag = function () {
        flagMorePost = 0;
        pageLoadHashTag = 0;
        flagLoadHashTag = 1;
        self.posts.removeAll();
        $('#msgHolder').css("display", "none");
        $("#loading").css("display", "block");
        var vl = {
            'page': pageLoadSortLike,
            'AccessToken': access_Token,
            'hashtag': txtHashTag
        };
        $.ajax({
            url: '../api/loadposthashtag/',
            dataType: "json",
            type: 'GET',
            data: vl
        })
        .done(function (data) {
            var mappedPosts = $.map(data, function (item) { return new Post(item); });
            self.posts(mappedPosts);
            flagLoadHashTag = 1;
            $('#loading').css('display', 'none');
            $('#msgHolder').css("display", "block");
            pageLoadHashTag++;
            nextPageLoadSort = data[data.length - 1].nextpage;
        })
        .fail(function () {
            error('unable to load posts');
        });
    };

    self.LoadMoreHashTag = function () {
        var count = pageLoadHashTag * 10;
        var postid = new Array();
        var likecount = new Array();
        var commentcount = new Array();
        for (var i = 0; i < 10; i++) {
            try {
                postid[i] = nextPageLoadSort.data[0].fql_result_set[i + count].post_id;
                likecount[i] = nextPageLoadSort.data[0].fql_result_set[i + count].likes.count;
                commentcount[i] = nextPageLoadSort.data[0].fql_result_set[i + count].comment_info.comment_count;
            }
            catch (ex) {

            }
        }
        if (postid.length != 0) {
            var vl = {
                'page': pageLoadHashTag,
                'postid': postid,
                'likecount': likecount,
                'commentcount': commentcount,
                'AccessToken': access_Token,
                'hashtag': txtHashTag,
            };
            //To load existing posts
            $.ajax({
                url: '../api/loadposthashtag/',
                dataType: "json",
                type: 'GET',
                data: vl
            })
            .done(function (data) {
                var mappedPosts = $.map(data, function (item) { return new Post(item); });
                $.each(mappedPosts, function (index, item) {
                    self.posts.push(item);
                })
                flagLoadHashTag = 1;
                $('#loadingMorePost').css('display', 'none');
                pageLoadHashTag++;
            })
            .fail(function () {
                error('unable to load posts');
            });
        }
        else {
            $('#loadingMorePost').css('display', 'none');
        }
    };

    self.addPost = function () {
        var post = new Post();
        post.Message(self.newMessage());
        return $.ajax({
            url: postApiUrl,
            dataType: "json",
            contentType: "application/json",
            cache: false,
            type: 'POST',
            data: ko.toJSON(post)
        })
        .done(function (result) {
            self.posts.splice(0, 0, new Post(result));
            self.newMessage('');

        })
        .fail(function () {
            error('unable to add post');
        });
    };

    ////for chorme
    //$(document).scroll(function () {
    //    if (/*$(this).innerHeight() + */$(this).scrollTop() + 1000 >= $(document.body).height()) {
    //for firefox
    $(window).scroll(function () {
        if ($(this).innerHeight() + $(this).scrollTop()  >= $(document.body).height()) {
            if (flagMorePost == 1) {
                $('#loadingMorePost').css('display', 'block');
                self.LoadMorePost();
                flagMorePost = 0;
            }
            else {
                if (flagLoadSortLike == 1) {
                    $('#loadingMorePost').css('display', 'block');
                    self.LoadMorePostSortLike();
                    flagLoadSortLike = 0;
                }
                else {
                    if (flagLoadSortComment == 1)
                    {
                        $('#loadingMorePost').css('display', 'block');
                        self.LoadMorePostSortComment();
                        flagLoadSortComment = 0;
                    }
                    else {
                        if (flagLoadHashTag == 1) {
                            $('#loadingMorePost').css('display', 'block');
                            self.LoadMoreHashTag();
                            flagLoadHashTag = 0;
                        }
                    }
                }
            }
        }
    });
    if (flagLoadHashTagorPost == 0) {
        self.loadPosts();
    }
    else {
        self.LoadHashTag(); 
    }
    self.getMe();
    return self;
        
};

function clickHT(tag) {
    flagLoadHashTagorPost = 1;
    flagMorePost = 0;
    flagLoadSortLike = 0;
    flagLoadSortComment = 0;
    flagLoadHashTag = 0;
    txtHashTag = tag.innerText;
    hashtag();
    vmd.LoadHashTag();
}
//custom bindings

//textarea autosize
ko.bindingHandlers.jqAutoresize = {
    init: function (element, valueAccessor, aBA, vm) {
        if (!$(element).hasClass('msgTextArea')) {
            $(element).css('height', '1em');
        }
        $(element).autosize();
    }
};
var vmd = new viewModel();
ko.applyBindings(vmd);

hashtag
function allpost()
{
    $("#allpost li").addClass('classChangeColorMenu');
    $("#sortlike li").removeClass('classChangeColorMenu');
    $("#sortcomment li").removeClass('classChangeColorMenu');
    $("#hashtag li").removeClass('classChangeColorMenu');
}
function sortlike()
{
    $("#sortlike li").addClass('classChangeColorMenu');
    $("#allpost li").removeClass('classChangeColorMenu');
    $("#sortcomment li").removeClass('classChangeColorMenu');
    $("#hashtag li").removeClass('classChangeColorMenu');
}
function sortcomment() {
    $("#sortcomment li").addClass('classChangeColorMenu');
    $("#sortlike li").removeClass('classChangeColorMenu');
    $("#allpost li").removeClass('classChangeColorMenu');
    $("#hashtag li").removeClass('classChangeColorMenu');
}
function hashtag() {
    $("#hashtag li").addClass('classChangeColorMenu');
    $("#sortlike li").removeClass('classChangeColorMenu');
    $("#allpost li").removeClass('classChangeColorMenu');
    $("#sortcomment li").removeClass('classChangeColorMenu');
}