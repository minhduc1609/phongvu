﻿function SetProduct(data) {
    var self = this;
    self.MaSanPham = data.MaSanPham || "";
    self.TenSanPham = data.TenSanPham || "";
    self.DiemBinhChon = data.DiemBinhChon || "";
    self.NhaSanXuat = data.NSXuat.TenNhaSanXuat || "";
    self.MoTa = data.MoTa || "";
    self.Gia = data.Gia + " VNĐ" || "";
    self.AnhMinhHoa = "http://serviceprovider.apphb.com/" + data.AnhMinhHoa || "";
    self.url = "/DetailProduct/Details/" + data.MaSanPham;
}
function viewmodel() {
    var self = this;
    var mansx;
    self.error = ko.observable();
    self.product = ko.observableArray();
    self.productsame = ko.observableArray();
    self.productsame2 = ko.observableArray();
    self.productrandom = ko.observableArray();
    self.productrandom2 = ko.observableArray();
    self.producthot = ko.observableArray();
    self.productfollowkind = ko.observableArray();
    self.loadProduct = function () {
        $.ajax({
            url: 'http://consumertest.apphb.com/api/consumerdetailproductapi/' + id,
            dataType: "json",
            type: 'GET',
        })
        .done(function (data) {
            var mappedProduct = new SetProduct(data);
            self.product.push(mappedProduct);
            mansx = data.NSXuat.MaNhaSanXuat;
            self.loadProductSame();                    
        })
        .fail(function () {
            error('unable to load posts');
        });
    };

    self.loadProductSame = function () {
        $.ajax({
            url: 'http://consumertest.apphb.com/api/consumerproductsameapi/6/' + mansx,
            dataType: "json",
            type: 'GET',
        })
        .done(function (data) {
            var mappedProduct = $.map(data, function (item) { return new SetProduct(item); });
            $.each(mappedProduct, function (index, item) {
                if (index > 2) {
                    self.productsame2.push(item);
                }
                else {
                    self.productsame.push(item);
                }
            })
            self.loadProductRandom();
        })
        .fail(function () {
            error('unable to load posts');
        });
    };
    
    self.loadProductRandom = function () {
        $.ajax({
            url: 'http://consumertest.apphb.com/api/consumerproductrandomapi/2',
            dataType: "json",
            type: 'GET',
        })
        .done(function (data) {
            var mappedProduct = $.map(data, function (item) { return new SetProduct(item); });
            self.productrandom2.push(mappedProduct[0]);
            self.productrandom.push(mappedProduct[1]);
            self.loadProductHot(); 
        })
        .fail(function () {
            error('unable to load posts');
        });
    };

    self.loadProductHot = function () {
        $.ajax({
            url: 'http://consumertest.apphb.com/api/consumerproducthotapi/3',
            dataType: "json",
            type: 'GET',
        })
        .done(function (data) {
            var mappedProduct = $.map(data, function (item) { return new SetProduct(item); });
            self.producthot(mappedProduct);
            initfb();
        })
        .fail(function () {
            error('unable to load posts');
        });
    };

    self.loadProductfollowkind = function () {
        $.ajax({
            url: 'http://consumertest.apphb.com/api/consumerproductsameapi/0/' + id,
            dataType: "json",
            type: 'GET',
        })
        .done(function (data) {
            var mappedProduct = $.map(data, function (item) { return new SetProduct(item); });
            self.productfollowkind(mappedProduct);
            self.loadProductRandom();
        })
        .fail(function () {
            error('unable to load posts');
        });
    };

    if (flagLoadPage == 101) {
        self.loadProduct();
        //initfb();
    } else {
        if (flagLoadPage == 103) {
            self.loadProductRandom();
            //initfb();
        }
        else {
            self.loadProductfollowkind();
            //initfb();
        }
    }
    return self;
}

ko.applyBindings(new viewmodel());