﻿function initfb() {
    (function (d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s); js.id = id;
        js.src = "//connect.facebook.net/en_US/all.js#xfbml=1&appId=565846006843207";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));
    window.fbAsyncInit = function () {
        FB.init({
            appId: '565846006843207', // App ID
            channelUrl: '//' + window.location.hostname + '/channel', // Path to your Channel File
            status: true, // check login status
            cookie: true, // enable cookies to allow the server to access the session
            xfbml: true  // parse XFBML
        });
        // listen for and handle auth.statusChange events
        if (sessionStorage.accesstoken != "") {
            if (sessionStorage.flag == "" || sessionStorage.flag == null) {
                $.ajax({
                    url: 'http://consumertest.apphb.com/api/consumeruserapi/' + sessionStorage.accesstoken,
                    dataType: "json",
                    type: 'GET',
                })
                .done(function (result) {
                    if (result == "-1") {
                        alert("Access Denied");
                    }
                    else {
                        document.getElementById('auth-displayname').innerHTML = result.Username;
                        document.getElementById('auth-displaypicture').innerHTML = "<img src='http://graph.facebook.com/" + result.ID + "/picture' width = 40 height = 40></img>";
                        sessionStorage.ID = result.ID;
                        sessionStorage.Username = result.Username;
                        $('#auth-loggedout').css('display', 'none');
                        $('#auth-loggedin').css('display', 'block');
                        $('#txtlogin').css('display', 'none');
                        $('#txtlogout').css('display', 'block');
                        sessionStorage.flag = "1";
                    }
                })
                .fail(function () {
                    error('unable to load posts');
                });
            }
            else {
                document.getElementById('auth-displayname').innerHTML = sessionStorage.Username;
                document.getElementById('auth-displaypicture').innerHTML = "<img src='http://graph.facebook.com/" + sessionStorage.ID + "/picture' width = 40 height = 40></img>";
                $('#auth-loggedout').css('display', 'none');
                $('#auth-loggedin').css('display', 'block');
                $('#txtlogin').css('display', 'none');
                $('#txtlogout').css('display', 'block');
            }
        }
        else {
            StartLoad();
        }
        //ko.applyBindings(new viewmodel());
        //FB.Event.subscribe("auth.logout", function () { window.location.reload(); });

        $("#txtlogout").click(function () { DangXuat(); });
    }

    function DangXuat() {
        $('#auth-loggedout').css('display', 'block');
        $('#auth-loggedin').css('display', 'none');
        $('#txtlogin').css('display', 'block');
        $('#txtlogout').css('display', 'none');
        $.get('/Home/ProcessLogout/', function (result) {
            sessionStorage.accesstoken = "";
            sessionStorage.flag = "";
            sessionStorage.ID = "";
            sessionStorage.Username = "";
            window.location = "http://serviceprovider.apphb.com/Home/Logout?redis=http://consumertest.apphb.com/"
        });
    }
    function StartLoad() {
        if (sessionStorage.accesstoken == "") {
            $('#auth-loggedout').css('display', 'block');
            $('#auth-loggedin').css('display', 'none');
            $('#txtlogin').css('display', 'block');
            $('#txtlogout').css('display', 'none');
        }
    }
}
